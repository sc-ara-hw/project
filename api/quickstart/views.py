from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from api.quickstart.models import Search
from api.quickstart.serializers import UserSerializer, GroupSerializer, SearchSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SearchViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Search.objects.all()
    serializer_class = SearchSerializer
