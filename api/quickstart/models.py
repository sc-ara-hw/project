from django.db import models


class Search(models.Model):
    name = models.CharField(max_length=100)
    matcher = models.CharField(max_length=8)
    status = models.CharField(max_length=30, default='CREATED')
    percent_complete = models.FloatField(default=0.0)
